<?php
$htmlbody = file_get_contents("body.html");
$htmlerfolg = file_get_contents("erfolg.html");


session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

if ($_POST) {
  
  $_SESSION['pd'] = $_POST;
  
  // Redirect to this page.
  header("Location: " . $_SERVER['REQUEST_URI']);
  exit();
    
} elseif (isset($_SESSION['pd'])) {

    //Create a new PHPMailer instance
    $mail = new PHPMailer();
    
    $htmlbody = <<<EOHTML
<h3>Bestellung</h3>
<p>Anzahl Klingel: {$_SESSION['pd']['anzahl']}<br/>
Anzahl Aufkleber: {$_SESSION['pd']['num_mitgliedaufkleber']}<br/>
Anzahl Gutscheine: {$_SESSION['pd']['num_gutscheine']}<br/>
Anzahl Flyer: {$_SESSION['pd']['num_flyer']}
</p>

<p>Versand oder Abholung: {$_SESSION['pd']['abholung']}
</p>

<p>{$_SESSION['pd']['vorname']} {$_SESSION['pd']['name']}<br/>
{$_SESSION['pd']['strasse']}<br/>
{$_SESSION['pd']['plz']} {$_SESSION['pd']['ort']}
</p>

<p>E-Mail: {$_SESSION['pd']['email']}
</p>

EOHTML;
    

    //Set who the message is to be sent from
    $mail->setFrom('info@adfc-sachsen.de', 'Klingelbestellsystem');
    //Set an alternative reply-to address
    
    $mail->addReplyTo($_SESSION['pd']['email'], "{$_SESSION['pd']['vorname']} {$_SESSION['pd']['name']}");
    //Set who the message is to be sent to
    //$mail->addBcc('nils.larsen@adfc-dresden.de', 'Nils Larsen');
    $mail->addAddress('klingeling@adfc-sachsen.de', 'ADFC Sachsen');
    //Set the subject line
    $mail->Subject = 'Klingelbestellung';
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $mail->msgHTML($htmlbody);
    //Replace the plain text body with one created manually
    //$mail->AltBody = 'This is a plain-text message body';
    
    $mail->setLanguage("de"); 
    $mail->CharSet = 'UTF-8';


    //send the message, check for errors
    if (!$mail->send()) {
        $htmlbody .= 'Fehler beim Senden der Bestellung: ' . $mail->ErrorInfo;
    } else {
        $htmlbody = file_get_contents("erfolg.html");
    }

    session_destroy();

}


?>


<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;1,400&display=swap" rel="stylesheet">
<link rel="stylesheet" href="klingeling.css">
<title>ADFC Sachsen - Deine Klingel zum Jubiläum</title>
</head>
<body>
<?php echo $htmlbody; ?>
<hr>
ADFC Sachsen e.V., Bautzner Straße 25, 01099 Dresden <br/><a href="https://www.adfc-sachsen.de">www.adfc-sachsen.de</a> &middot; E-Mail: info@adfc-sachsen.de
</body>
</html>
